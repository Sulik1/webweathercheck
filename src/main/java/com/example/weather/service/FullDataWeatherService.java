package com.example.weather.service;

import com.example.weather.model.FullDataWeather;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
@Service
public class FullDataWeatherService {
    private static final String ADDRESS = "http://api.apixu.com/v1/current.json";


    public FullDataWeather getDataByCity(String city) throws UnirestException, IOException {
            configureJacksonObjectMapper();
            String ci = city.replace(" ","");
            String dataJson = Unirest.get(ADDRESS + "?key=e12ed81262e74d4d94e175249192504").queryString("q",city).asString().getBody();
            ObjectMapper mapper = new ObjectMapper();
            FullDataWeather data = mapper.readValue(dataJson, FullDataWeather.class);
            if (data==null||data.getLocation()==null||data.getLocation().equals("")){
                return null;
            }
            //todo dto
            return data;


    }


    public static void configureJacksonObjectMapper(){
        Unirest.setObjectMapper(new com.mashape.unirest.http.ObjectMapper() {
            com.fasterxml.jackson.databind.ObjectMapper mapper = new
                    com.fasterxml.jackson.databind.ObjectMapper();
            @Override
            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return mapper.readValue(value,valueType);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                throw  new RuntimeException("Could nod read object forms string");
            }

            @Override
            public String writeValue(Object value) {
                try {
                    return mapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }

                throw  new RuntimeException("Could not convert object to json");
            }

        });
    }

}
