package com.example.weather.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Current {
    @JsonProperty("temp_c")
    private double temp;
    private Condition condition;
    @JsonProperty("pressure_mb")
    private double pressure;

    public Current() {
    }

    public Current(double temp,  Condition condition,double pressure) {
        this.temp = temp;
        this.pressure=pressure;
        this.condition = condition;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public Condition getCondition() {
        return condition;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    @Override
    public String toString() {
        return "Current{" +
                "temp=" + temp +
                ", condition=" + condition +
                ", pressure=" + pressure +
                '}';
    }
}
