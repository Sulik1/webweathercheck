package com.example.weather.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Condition {
    @JsonProperty("text")
    private String textCondiotion;

    public Condition() {
    }

    public Condition(String textCondiotion) {
        this.textCondiotion = textCondiotion;
    }

    public String getTextCondiotion() {
        return textCondiotion;
    }

    public void setTextCondiotion(String textCondiotion) {
        this.textCondiotion = textCondiotion;
    }

    @Override
    public String toString() {
        return "Condition{" +
                "textCondiotion='" + textCondiotion + '\'' +
                '}';
    }
}
