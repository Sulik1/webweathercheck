package com.example.weather.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FullDataWeather {
    private Location location;
    private Current current;



    public FullDataWeather() {
    }

    public FullDataWeather(Location location, Current weather) {
        this.location = location;
        this.current = weather;

    }



    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Current getCurrent() {
        return current;
    }

    public void setCurrent(Current current) {
        this.current = current;
    }

    @Override
    public String toString() {
        return "FullDataWeather{" +
                "location=" + location +
                ", current=" + current +
                '}';
    }
}
