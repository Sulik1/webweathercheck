package com.example.weather.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Day {
    @JsonProperty("maxtemp_c")
    private double maxTemp;
    @JsonProperty("mintemp_c")
    private double minTemp;
    @JsonProperty("maxwind_mph")
    private double maxWind;
    private Condition condition;

    public Day() {
    }

    public Day(double maxTemp, double minTemp, double maxWind, Condition condition) {
        this.maxTemp = maxTemp;
        this.minTemp = minTemp;
        this.maxWind = maxWind;
        this.condition = condition;
    }

    public double getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(double maxTemp) {
        this.maxTemp = maxTemp;
    }

    public double getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(double minTemp) {
        this.minTemp = minTemp;
    }

    public double getMaxWind() {
        return maxWind;
    }

    public void setMaxWind(double maxWind) {
        this.maxWind = maxWind;
    }

    public Condition getCondition() {
        return condition;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    @Override
    public String toString() {
        return "Day{" +
                "maxTemp=" + maxTemp +
                ", minTemp=" + minTemp +
                ", maxWind=" + maxWind +
                ", condition=" + condition +
                '}';
    }
}
