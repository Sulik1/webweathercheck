package com.example.weather.view;

import com.example.weather.model.FullDataWeather;
import com.example.weather.model.Location;
import com.example.weather.service.FullDataWeatherService;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

@Controller
public class DataView {
    @Autowired
    FullDataWeatherService fullDataWeatherService;
    @GetMapping("/search")
    public ModelAndView searchCity(){
        ModelAndView modelAndView = new ModelAndView("searchForm");
        Location location = new Location();
        modelAndView.addObject("data",location);
        return modelAndView;

    }
    @PostMapping
    public ModelAndView searchHandler(@ModelAttribute Location location) throws IOException, UnirestException {
        FullDataWeather dataByCity = fullDataWeatherService.getDataByCity(location.getCity());
        if (dataByCity==null||dataByCity.getLocation()==null){
            Location location1 = new Location();
            ModelAndView modelAndView1 = new ModelAndView("searchForm");
            modelAndView1.addObject("data",location1);
            return modelAndView1;
        }
        ModelAndView modelAndView = new ModelAndView("fullData");
        modelAndView.addObject("dataFull",dataByCity);
        return modelAndView;
    }
    @GetMapping("/searchHandler")
    public String redirectToSearch(){
        return "redirect:/search";
    }
}
