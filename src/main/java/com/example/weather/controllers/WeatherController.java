package com.example.weather.controllers;

import com.example.weather.model.FullDataWeather;
import com.example.weather.service.FullDataWeatherService;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/api/v1/weather")
public class WeatherController {
    @Autowired
    FullDataWeatherService fullDataWeatherService;

    @GetMapping("/{city}")
    public FullDataWeather getWeather(@PathVariable String city) throws IOException, UnirestException {
        return fullDataWeatherService.getDataByCity(city);
    }

}
